# Views RSS SmartNews

Extension module for the [Views RSS
module](https://www.drupal.org/project/views_rss) that provides additional
options for the [SmartNews service](https://www.smartnews.com).

The specification is available from SmartNews' site:

- https://publishers.smartnews.com/hc/en-us/articles/360036526213-SmartFormat-Specification-Version-2-1-


## Requirements

Views RSS 2.x.


## Validating the feed

Once a feed has been created it can be validated using [SmartNews's
validator](https://sf-validator.smartnews.com/).


## Known issues

- The [W3C RSS validator](https://validator.w3.org/feed) reports an error with
  the namespace. This is because the W3C validator doesn't currently list
  SmartNews's namespace. As a result it is worth using the SmartNews validator
  noted above instead.
